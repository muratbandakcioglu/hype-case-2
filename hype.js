ALEF = {
	init: () => {
		ALEF.notify();
		ALEF.style();
		ALEF.playButton();
		// ALEF.close.click();
		ALEF.close.esc();
	},
	options: {
		className : "alef",
		image: '<svg id="fix_1" data-name="fix 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M437.33,512H74.67a32,32,0,0,1-32-32V266.67A10.67,10.67,0,0,1,53.33,256H458.67a10.67,10.67,0,0,1,10.66,10.67V480A32,32,0,0,1,437.33,512ZM64,277.33V480a10.7,10.7,0,0,0,10.67,10.67H437.33A10.7,10.7,0,0,0,448,480V277.33Z" transform="translate(0 0)" fill="#ffffff"/><path d="M480,277.33H32a32,32,0,0,1-32-32v-64a32,32,0,0,1,32-32H480a32,32,0,0,1,32,32v64A32,32,0,0,1,480,277.33ZM32,170.67a10.7,10.7,0,0,0-10.67,10.66v64A10.7,10.7,0,0,0,32,256H480a10.7,10.7,0,0,0,10.67-10.67v-64A10.7,10.7,0,0,0,480,170.67Z" transform="translate(0 0)" fill="#ffffff"/><path d="M256,170.67a10.68,10.68,0,0,1-10.56-12.16C245.67,156.91,269.53,0,394.67,0c55.12,0,74.68,29.78,74.68,55.27,0,47.92-66.19,115.4-213.35,115.4ZM394.67,21.33C306.94,21.33,277,114.88,269,149.12c116.89-3.63,179-55.81,179-93.85,0-9.47-4-17.28-11.82-23.14-9.38-7.06-23.76-10.8-41.53-10.8Z" transform="translate(0 0)" fill="#ffffff"/><path d="M256,170.67c-147.16,0-213.35-67.48-213.35-115.4C42.67,39.19,49.9,24.9,63,15.06S94.91,0,117.35,0c125.15,0,149,156.91,149.23,158.51A10.66,10.66,0,0,1,256,170.67ZM117.35,21.33C84.44,21.33,64,34.33,64,55.27c0,38.06,62.14,90.24,179.09,93.87C235.35,114.69,206.19,21.33,117.35,21.33Z" transform="translate(0 0)" fill="#ffffff"/><path d="M256,512a10.67,10.67,0,0,1-10.67-10.67V160a10.67,10.67,0,0,1,21.34,0V501.33A10.67,10.67,0,0,1,256,512Z" transform="translate(0 0)" fill="#ffffff"/></svg>',
		image2: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEMAAAA0CAYAAADR7YUKAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAA/pSURBVHgBtVsLyK5FEX5mv8/0VJZ2pcKyEOx+vxlUUnTBQKKIIoqM6EaRFRjZBTXRyCIzsLwkYoaSooGaFZpImZUISVaIWYmXrNCwTO2k7vbuzPPM7ndQ8XZe+M///d/7vruzM888Mzuzx8DruW/++N4GO8haeyHQdll+0JbL0KzV2rDcXL5avq79Dqz5E8s//cvK3/B3ln/M4v3+DTjWMkB/3d+PcX38ZahWW9yP7/w2Yq4xZo3v/K/+Osfo49fWOM8Yc5F5ESLH7vdjLo3fNC410LDqv56/zycPWt470aztbmZbdHf5HAL0Ufvn/nLpH4yDx/B8IBZZiknB8CljHAN101/huP23L4ZTdBVbXODEPkm8Usawm2OAgsS//A2bBeegIZNtO45xlNUL9/n0fsvtIy2Fy1W6/Bov1BkDUrXWNBcXrvsmIfU1tHQLdFFsR1iOQSlTDzE9rHDONI5s4UPS7HmLb+vZNozCuTRHPJ+K6QtaL5Z+X7/THOocpBHKiyByBX2WrII6GlIFTZDTQvrTpbhruF/1bzpyfJH97+JrpMhIV9Tal2djsUJm5dj91fjgugqvoVtg0ivh5esq8dD43hVgKHT32tbLaHtTzfzxmVwqx4PPX8LnOZwPvDhqarW5bkDhaVFO1GqYhIr2v4uZuMaEEFRpgZoNNS9vWPJOB6PrrkrSZcxVN2QTJkzyUIM+Rx/Fea9/VYfym8bor5UFGUJK61aqsfAQzAJ/sn31vzo+aq3xyTXTQuAgpgQGEsmBhMBCC8WkrEQJBu8Yn+zAa+TFpvvyRfJHCXIf7qHFUdmueJFHILt1AyWh032ElXVYvS+uM3Mh5dOl4ZaMxTWXkIwWA1pGmBhUft0G7N3jSAjBOdaoN1rTbAZuaL82kS4VuqKLFEaLkKuJN2fLo04EXoyKdYXHe5gcydFL48DWoUznUlmYqKaB6ePhGhkuqSiQSPuDq+WJyvtaXr9bOXfNhdBU1NbgI5CfSllxsYl7mTEJHIm4mD5dNPmXliDRGcKdtLBGS6UFlntrMbnA6HetkBwlw6A4o8aZNyhoYsQYClKrGJQjFKQ7hY+Ozy05N2SclN/dM6UIHqMLNFlLbwYVCWOBJHKREf3Ls6U68o3cSErwe2t+0RSkZU+5UtsMIfxOPs4UCOL74Y+KwsnoGPSsP4TXhoGeYZhQVKNnxioDtbVBkaPloKCrur83E+kLQSmPCUF8xQkmlNcJtJUO8LBkUy7QBiEw1ObEbm3lWDF9bfJBKe9Ze+xmh3/mfdj10Y/CRZf8Dt8++Wxcf8ON2PTXQabxq2QoYL4FIsilEHdkutAVomiruNoz1XCRVGKwvIfQiGoLYGrnpRKJlWRYPfnZrz1YxBf5BpFgExxMSYwlByIzPsgbhozLpyMO3A/P2XMPbNmyE/bc46l4+z6vxq233obfX3kNNsO4jWRIsMwgOVRnUzI2UkxL7km1Zh5llmMbMwUx5WZGmigpDcpYS/iYBdwFEH5PH6IgztIWiYebMBRMMu6KWxTxDMzXlp12woGfeC+O+8r+eOLjd0W8XyhzjoMRXbSQAtCy49mxaCqjYVJQatQ5nvcKGa6UjGA+ZhtyFM8QY5EUpuRgelGgQILDZs1zL8HJLEL6TjvuiLu7XvGS5+HM7xyCfd/wCmb/IWyfq8sCKmAQO+XAWLRDflNWN47RQEJLCaMFj9DYUECyaY0cs3CycCRadYYiEaZJ5TOy2KbykOMY7uXa+ZGPwGGf/RCOPPij9thddzaNG/MJASXCuimVsUZEpnyBXhmpWESgUFSsKRI7s0mZXTFDua4ozt+fLBQCNi+sCQllwDXyAWkZPiGCoSEYJ4ruw/X6V78UZ57wJbzuVS+g7mc3ASOczx1kV4oQ2BUlt4YyUpF8rmlCCce2gOOEcogtl0CSfscJE6YDlsNfkVptUpwnO/LN2b/v4/WYXR6Now7dH4cdsB92fsQWWW9y0U0EJuRDcEdMk0H8nUlhHkEgyyeSpCS+A/qLqxaRhnObYCu6SvpvarLlIGYSOslLhJYEdf+ufd/8GpyxcMleL35mElxLjohUQPIk+YlrhhvEgme3CwtTvkKuH+vzqB1c1dPCorwjtGqqqPSflTQrtjUhYCAkSdSCvbXTuv/Xk57wOBz71QNw4MfetUSfh8W22yKihe9vQD5RtPEjf6MLDSMWAquIbJOHqPTIDkKDqzbDKDTXP64g64uQMEKTJdmlwEWO+oCu/uq73/ZGnH7sQfaCZz8dmObrOd4cSu8hrMqNMjrOcnKjKYMb3WSKJk5IEQQKw9tElJa5Bybtj5BlNsdtF6g8IGTM19N2ezJOOupz+MT734oddliL9WPOEpYfqKTvTxEoNm6uXgzjmc3uDrk93+msGTD0fX5pnmFHeEuXCI3quTF42UzWkOT7IJAxX6vVCh98z7449ejPY8+nPyW5gUWUJMzZQBOXuYLM5ggng5Uk+0wbegZqI5XbGNxIOHFNyE+C1UB6hGjR54fw2nOP3XHqMQfhA+96U1eQTdENI0OVi0zkvskjbSR1ZlCypvypRxOF1RENlDMVsTfvu6qzRJbukfeLrPKACfTeroftsAM++aF34pgv77+Q645TNENuC0opWWF32TFQEBs1ImoOraEsfyVJYSahwRUbCoiN4QZ7F2WCQ9MmjW+fa6+XPR+f+uDboP0SWdBy/5S5xoQCylTSzTDcI128gHnG7FeliQTJylTIFK7m3GL8jARtOyBjvt7y+r3CQcP/VW4cnIGNTSTkVkrrbamk5fdKBZa/16MatHl1/Uat1Qs2AUt2tforLOCz0BNF4Wg3eEK63ZCRl0UFP8uWXJaA6pUtDFIziBq91+JRUy0XV+LyeS0Lh3/VqBVBVc6u5WpZ9yP6F520KLmHRiL+g163/RVxzvm/CllUWFwag1HmEyYbRtVNhi6sade5aE2Txj/r0CIbKdJ2G4lk85JHbPb6Z6+NlqhpglWMEpU2qqzvryq213XxpZfjGyeeJfIMOtPivUpWo/DE6jnLfy1LP1mTDUX2xTTW59Yarnp7ppKFG1PUpNXsVG1bEx11dFYM/cHCWt5Dd91xxx04+qSzcNIZF+CuWi37O1EZpqXZBfReg5BBaXv0k06AqcIYeXdf7jpCT40OQ3UUWOYXhB6RN/oc/F4uFvLUVMRDzRlX/OkafOFrJ+OPf7mBXq7mE6tsAX2wUh7pIFRLxdyVDCOpEcAkguvzgnAsvjV5CQEFDJJApOv99mIVeP+0OW+Ig6IVkL2OhwQZd91VceJpP8Exp56HO++8a/RpmzhzLB555gE0UE1HcH4L/MunTAEWlipie3HqMo0+TJvJmIXwaqkxJV/iGCMRKfI8yOvqa/+GLx55Ci6/8jrQ8mqKaGwztWlN7QaKqr6LqW2hDmDhsY6hAI3ZhWY0oV913ijRGgjz1tR08Ehpo5SfKGJJXpOOUv4Dubrw3z/7Zzjquz/G7Vv/ByaBNprZmpsCuwQ1E7+IhkHsorPs1bbh3aFVi/SoBmIYTSKEmrFsBEYNMFUPjoj7NTZ9leE1LFaj4uR5SGmY+53347rhHzfh4G+ehksuv5oQLUF0tcqKCf1IeyB3AZRX2oAOV4xsHFl+N7kKImEMzoi2Xxvyt+JNFohE1NXCiL7dS2t86Uhx0lSYic3e/bzOPv/XOOKEH+HW27cmVSmT8A1WnDnaPH5g6RN8HAMNSXwZageHEFHqDLqyllWu800rzKsqDyYF4TQ152l9/w6j82aDjyPpauLm++YqN938bxz2rR/gwkuvCjQy5oVyGxdhw5ERoZPMyLDPZm9IQVJlt195gIYQQnSARThq4ozYovhccbbMWiZZmW44HxgzVXeHcKVMR+KX1W1wes/XTy/+LQ4/7lzcfMttkid5oeVpAIZJ2jDOiQRSoFMOonz/Z4MZo/gU4dCA0eierjwWsw4I1uzAF09bq4U/bjORfBOT+wTXiDYsMtZ7z0BvWdqMRxx/Ds79xRUxXll15MWRhYBfG4dhMuEDRtBjuPKJqbUavh8Gat6TZ8del9Ol0nYlknSd5seY/E8tLtrI8zm/VDaQGYrw3yMPTxFy8yZEGbZu3Yod76ar9svLrsShx/wQf//nrRye+58El7snPxqPMOaZMxB9iW3kYROSWSwsMw6Fvc41cTxErjyVXtitX883BxK4VxlBKW3C4wMtXKpmsOfghGDBH666Hi96zjNSCbf/dyuOOvk8nHHB5ekCaTDOJ7fTaRqOz0MucQyJGfIIn2ibKcM4LpHHGBhkkduFmc2ml9fppFi12Hg0ZWwtjwRxE9YGYwpf0XewhFwGq6+f8nMc8uEt2PVRD8dFv/kzjj/zYvz1xv8Eij2N1thtxHubUE2lOt7yRBGUOA7ft9lVWojtOy3tn4N3s5qBiVamy6Xa6yPfq7JyIkT7WycOZlY8kQuy9OazLY9w8MfyCFHTjnc6RewnelkbQZ7xkmk3x1WWvSEfmG7Tb3gvTwdjUz6dH6uNShrRpknJzesZcxYN6VOCxaZrBKjK3kW1DetIteAJ5mDFcBulItBhM+6aSMaVSZyxYhURa+hFuYTCoC8oSxA8Spkhsk1QG65j8x6mFyVY4NEJxhrKiqQrJ3MLInL4RDzZ17PPKS0vdWM7FgQcO2DCt6UXAFNmWAj3SuIVw4dbtjiCaclFsSMO3siCZixQexAdiO1XjTPj8sCJkE2GMIqEkWeEktaRL3Cr3mwshmVFt0BaP5DiYYN5YIY4DwqufZBfbHCyFJb7nRZ8UG0zXBYycB7CpUJWTa6ms1p+hkr8FWBw48XOtk5bKC96uiKYyQIDeSMny+KOqtu1pnUzpd3G+oN5CuWR7yE3tMC2uQ2tI9Tp3BVGtsnMcCo5am7VYB0NyHOp2rx543hlM8fknkG7Vo0zZZ8Ehiu4cLHdff4FiMVLUz9BvQZky3B0n9i196p57EvMxpmurq8VLW9Q/4W+FrGxzE2gvIescBsr9DbnHtoupDz5Hkw2MkxzjfFt89no3BsJt5BOvZppl0kQ9TDbaOaqjYfwSDWWPDfIEwpcAJUUC4le79RaiEVYolDHIPTOhsDYONJEIxE33ZvKNGYhCiMGI7traEMhU5tRWYCvazXGtCjtHuKWTI2HzNRiCD41Wvh35BazZWgFKad5oTXKcpY6sERf6Ho8L4u30a/B6IYFCi2PTxiPRMQ58TwdMBDhxyQT2Zy76ShU9pADrZJvdd0lp1+928vf0b/dO9GKgdCEH+2ayoJoh9xnGw+ZVDrGyDENiREqcWBcCVybbkoOmTu37JGm22Dgae7hYojXphkHupHG7y7j/xPp2ktOv/Apr3zn1Ysddln+3D0hm+7SbNJOG4uOkJ/EmQtJxbSxDKJE4+b6PSbZLGCKPNEG8kwGI2zjeCMFafPioOCIaeHDKNJJPt+/+z9BjVZnUz7b5QAAAABJRU5ErkJggg==',
		escKeyClick: true
	},
	style: () => {
		const css = `.${ALEF.options.className}-notify {
				align-items: center;
				display: flex;
				width: 100%;
			}
			.${ALEF.options.className}-notify-content {
				align-items: center;
				background: rgb(59,111,175);
				background: linear-gradient(90deg, rgba(59,111,175,1) 0%, rgba(35,37,48,1) 100%);
				border-radius: 4px;
				color: #fff;
				display: flex;
				font-size: 18px;
				font-weight: 300;
				padding: 13px 20px;
			}
			.${ALEF.options.className}-notify-content a {
				color: #fff;
				font-weight: 600;
			}
			.${ALEF.options.className}-notify-content svg {
				display:  inline-block;
				margin-right: 10px;
				max-width: 36px;
			}

			.${ALEF.options.className}-play {
				background-color: #fff;
				border-radius: 5px;
				color: #525252;
				display: inline-block;
				font-size: 23px;
				overflow: hidden;
				padding-right: 23px;
			}

			.${ALEF.options.className}-play img {
				margin-right: 23px;
			}
			.${ALEF.options.className}-overlay {
				align-items: center;
				background-color: rgba(0, 0, 0, .75);
				display: flex;
				height: 100vh;
				justify-content: center;
				left: 0;
				position: fixed;
				top: 0;
				width: 100vw;
				z-index: 99;
			}
			.${ALEF.options.className}-popup {
				background-color: #232530;
				border-radius: 5px;
				height: 465px;
				max-width: 600px;
				position: relative;
				width: 100%;
			}
			.${ALEF.options.className}-popup iframe {
				border: 0;
				height: 100%;
				width: 100%;
			}
			.${ALEF.options.className}-popup-close{
				color: #666;
				font-size: 25px;
				font-weight: bold;
				position: absolute;
				right: 12px;
				text-decoration: none;
				transition: .5s;
				top: 0;
			}
			@media screen and (max-width: 600px) {

			}`,
			style = document.createElement('style');
		style.innerHTML = css;
		document.head.appendChild(style);

		// .headline-section
	},
	notify: () => {
		const html = `<div class="${ALEF.options.className}-notify-content row">
					${ALEF.options.image}
					Alef'i yakından takip etmene çok sevindik, şimdi abone olursan,&nbsp;<a href="javascript:;" class="${ALEF.options.className}-popup-opener">1 hafta abonelik de bizden hediye!</a>
				</div>`,
			div = document.createElement('div'),
			parent = document.querySelector(".headline-section .container"),
			firstChild = document.querySelector(".headline-section .container .row");
			div.setAttribute('class', ALEF.options.className + "-container");
			div.innerHTML = html;

		parent.insertBefore(div, firstChild);
		document.querySelector("." + ALEF.options.className + "-popup-opener").addEventListener("click", ALEF.modal);
	},
	playButton: () => {
		const html = `<a href="javascript:;" class="${ALEF.options.className}-play"><img src="${ALEF.options.image2}" alt="Play" />Fragmanı Oynat</a>`,
			div = document.createElement('div'),
			parent = document.querySelector(".content-wrapper");
		div.setAttribute('class', 'buttons-wrapper');
		div.innerHTML = html;
        document.querySelector('.content-wrapper').appendChild(div);
		document.querySelector("." + ALEF.options.className + "-play").addEventListener("click", (e) => {
			ALEF.videoPlay();
		},false);	
		
		ALEF.removeExButton();

	},
	videoPlay: () => {
		document.querySelector('.has-trailer').click()
	},
	removeExButton: () => {
		const exButton = document.querySelector('.content-wrapper .buttons-wrapper')
		exButton.parentNode.removeChild(exButton);
	},
	modal: () => {
		const html = `<div class="${ALEF.options.className}-popup">
				<a href="javascript:;" onClick="ALEF.close.action();" class="${ALEF.options.className}-popup-close">&#215;</a>
				<iframe src="https://www.blutv.com/iletisim-izinleri"></iframe>
			</div>`,
			div = document.createElement('div');
		div.setAttribute('class', ALEF.options.className + "-overlay");
		div.innerHTML = html;
		document.body.appendChild(div);

	},
	close: {
		click: () => {
			document.querySelector("." + ALEF.options.className + "-popup-close").addEventListener("click", ALEF.close.action());
		},
		esc: () => {
			if(ALEF.options.escKeyClick === true) {
				document.addEventListener('keydown', function(e) {
					const keyCode = e.keyCode;
					if (keyCode === 27) {
						ALEF.close.action();
					}
				});
			}
		},
		action: () => {
			ALEF.removeModal();
		}
	},
	removeModal: () => {
		const el = document.querySelector('.' + ALEF.options.className + "-overlay" );
		el.parentNode.removeChild(el);   
	}
}
ALEF.init();
